##Build
FROM java:8 as build
VOLUME /tmp
ADD . .
RUN ./mvnw install -Dmaven.test.skip=true

##Run
FROM java:8
WORKDIR /app
COPY --from=build target/bars-service-package.jar .
EXPOSE 5000
ENTRYPOINT [ "sh", "-c", "java   -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=production -jar bars-service-package.jar"]