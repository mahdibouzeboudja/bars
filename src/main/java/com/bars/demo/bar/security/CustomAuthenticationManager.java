package com.bars.demo.bar.security;

import com.bars.demo.bar.model.User;
import com.bars.demo.bar.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class CustomAuthenticationManager implements AuthenticationManager {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    public CustomAuthenticationManager(UserRepository userRepository,PasswordEncoder passwordEncoder,JwtTokenProvider jwtTokenProvider){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        User user = this.userRepository.findByUsername(name);
        if (user != null && passwordEncoder.matches(password, user.getPassword())) {
            String token = this.jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
            return this.jwtTokenProvider.getAuthentication(token);
        }else {
            throw new BadCredentialsException("Invalid username/password");
        }
    }
}
