package com.bars.demo.bar.mapper;

import com.bars.demo.bar.dto.UserDTO;
import com.bars.demo.bar.model.User;
import com.bars.demo.bar.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserMapper {

    private UserRepository userRepository;

    @Autowired
    public UserMapper(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public UserDTO userToUserDTO(User user){
        return new UserDTO(user);
    }

    public User userDTOTOUser(UserDTO userDTO){
        if(null == userDTO){
            return null;
        }else{
            User user = new User();
            user.setId(userDTO.getId());
            user.setUsername(userDTO.getUsername());
            user.setEmail(userDTO.getEmail());
            user.setRoles(user.getRoles());
            user.setFriends(
                    userDTO.getFriends() != null && !userDTO.getFriends().isEmpty()
                            ? userDTO.getFriends().stream()
                            .map(
                                   id -> this.userRepository.existsById(id) ? userRepository.findById(id).get(): null
                            )
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList())
                            : null

            );
            return user;
        }
    }
}
