package com.bars.demo.bar.dto;

import com.bars.demo.bar.model.Role;
import com.bars.demo.bar.model.User;
import lombok.Data;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class UserDTO {

    private long id;

    private String username;

    private String password;

    private String email;

    private Collection<Long> friends;

    private List<Role> roles;

    // Default constructor.
    public UserDTO(){}


    public UserDTO(User user){
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getUsername();
        this.email = user.getEmail();
        this.friends = friends != null && !user.getFriends().isEmpty()
                ? user.getFriends().stream()
                .map(User::getId)
                .collect(Collectors.toList())
                : null;
        this.roles = user.getRoles();
    }
}
