package com.bars.demo.bar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class BarsApplication {


    public static void main(String[] args) {
        SpringApplication.run(BarsApplication.class, args);
        //https://www.getpostman.com/collections/802cc888aaa2085cd33d
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @GetMapping("/")
    ResponseEntity healthCkeck(){
        return ResponseEntity.ok().body("Spring running good!");
    }
} 
