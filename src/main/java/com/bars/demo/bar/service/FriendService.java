package com.bars.demo.bar.service;

import com.bars.demo.bar.model.Friend;

import java.util.List;

public interface FriendService {

    Friend getFriendById(Integer friendId);

    void addFriend(Friend friend);

    List<Friend> getAllFriend();
}
