package com.bars.demo.bar.service;

import com.bars.demo.bar.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {

    HTTPRequestService HTTPRequestService;
    JSONHelper jsonHelper;

    @Autowired
    public EventService(HTTPRequestService HTTPRequestService, JSONHelper jsonHelper){
        this.HTTPRequestService = HTTPRequestService;
        this.jsonHelper = jsonHelper;
    }

    public List<Event> getAllEvents(){
        String response = "";

        try {
            response = HTTPRequestService.getRequest("https://opendata.paris.fr/api/records/1.0/" +
                    "search//?dataset=que-faire-a-paris-&q=bar");
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Event> events = jsonHelper.eventsListFromJSONString(response);

        return events;
    }

    public List<Event> getEventsByGeolocation(double latitude, Double longitude, int distance){
        String response = "";

        try {
            response = HTTPRequestService.getRequest("https://opendata.paris.fr/api/records/1.0/search//" +
                    "?dataset=que-faire-a-paris-&q=bar&geofilter.distance="+ latitude +", "+ longitude +", "+ distance);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Event> events = jsonHelper.eventsListFromJSONString(response);

        return events;
    }

    public Event getEventsById(int id) {
        String response = "";

        try {
            response = HTTPRequestService.getRequest("https://opendata.paris.fr/api/datasets/1.0/" +
                    "que-faire-a-paris-/records/"+id);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Event event = jsonHelper.eventFromJSONString(response);

        return event;
    }
}
