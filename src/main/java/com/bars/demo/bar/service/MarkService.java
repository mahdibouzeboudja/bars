package com.bars.demo.bar.service;

import com.bars.demo.bar.model.Mark;
import java.util.List;

public interface MarkService {

    Mark getMarkById(Integer markId);

    void addMark(Mark mark);

    List<Mark> getAllMark();
}
