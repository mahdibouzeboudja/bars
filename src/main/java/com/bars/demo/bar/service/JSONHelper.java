package com.bars.demo.bar.service;

import com.bars.demo.bar.model.Event;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JSONHelper {

    public List<Event> eventsListFromJSONString(String jsonStringEvents){
        JSONParser parser = new JSONParser();
        JSONObject jsonObject;

        List<Event> events = new ArrayList<>();

        try {
            jsonObject = (JSONObject) parser.parse(jsonStringEvents);
            JSONArray jsonArray = (JSONArray) jsonObject.get("records");

            for(int i = 0; i < jsonArray.size(); i ++){
                JSONObject object = (JSONObject) ((JSONObject) jsonArray.get(i)).get("fields");

                int id = Integer.parseInt((String) object.get("id"));
                String name = (String) object.get("title");
                String eventStartDate = (String) object.get("date_start");
                String eventEndDate = (String) object.get("date_end");
                String eventDescription = (String) object.get("description");
                String address = (String) object.get("address_street");
                String city = (String) object.get("address_city");
                int zipcode = Integer.parseInt((String)object.get("address_zipcode"));
                String priceDetail = (String) object.get("price_type");
                String transportsDetails = (String) object.get("transport");
                String contactUrl = (String) object.get("contact_url");
                String coverUrl = (String) object.get("cover_url");

                Event event = new Event(id, name, eventStartDate, eventEndDate, eventDescription, address, city,
                        zipcode, priceDetail, transportsDetails, contactUrl, coverUrl);

                events.add(event);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return events;
    }

    public Event eventFromJSONString(String jsonStringEvents) {
        JSONParser parser = new JSONParser();
        JSONObject object;

        List<Event> events = new ArrayList<>();

        try {
            object = (JSONObject) parser.parse(jsonStringEvents);

            int id = Integer.parseInt((String) object.get("id"));
            String name = (String) object.get("title");
            String eventStartDate = (String) object.get("date_start");
            String eventEndDate = (String) object.get("date_end");
            String eventDescription = (String) object.get("description");
            String address = (String) object.get("address_street");
            String city = (String) object.get("address_city");
            int zipcode = Integer.parseInt((String)object.get("address_zipcode"));
            String priceDetail = (String) object.get("price_type");
            String transportsDetails = (String) object.get("transport");
            String contactUrl = (String) object.get("contact_url");
            String coverUrl = (String) object.get("cover_url");

            return new Event(id, name, eventStartDate, eventEndDate, eventDescription, address, city,
                    zipcode, priceDetail, transportsDetails, contactUrl, coverUrl);


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
