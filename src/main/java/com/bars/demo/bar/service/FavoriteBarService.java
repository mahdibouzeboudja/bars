package com.bars.demo.bar.service;

import com.bars.demo.bar.model.FavoriteBar;
import com.bars.demo.bar.model.User;
import com.bars.demo.bar.repository.FavoriteBarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoriteBarService {

    FavoriteBarRepository favoriteBarRepository;

    public FavoriteBarService(){}

    @Autowired
    public FavoriteBarService(FavoriteBarRepository favoriteBarRepository){
        this.favoriteBarRepository = favoriteBarRepository;
    }


}