package com.bars.demo.bar.model;
import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="users_table")
@Data
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @NotNull
    private String username;
    @NotNull
    private String password;

    @NotNull
    private String email;

    @OneToMany(mappedBy="id", cascade=CascadeType.ALL)
    private Collection<User> friends;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles;

    //Default constructor.
    public User(){};

    public User(@NotNull String username,@NotNull String password,@NotNull String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }


    public void addFriend(Friend friend) {
    }
}
