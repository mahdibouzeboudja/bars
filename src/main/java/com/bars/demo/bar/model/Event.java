package com.bars.demo.bar.model;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Event {

    private int id;
    private String name;
    private String eventStartDate;
    private String eventEndDate;
    private String eventDescription;
    private String address;
    private String city;
    private int zipcode;
    private String priceDetail;
    private String transportsDetails;
    private String contactUrl;
    private String coverUrl;


    public Event(int id, String name, String eventStartDate, String eventEndDate, String eventDescription,
                 String address, String city, int zipcode, String priceDetail, String transportsDetails,
                 String contactUrl, String coverUrl) {
        this.id = id;
        this.name = name;
        this.eventStartDate = eventStartDate;
        this.eventEndDate = eventEndDate;
        this.eventDescription = eventDescription;
        this.address = address;
        this.city = city;
        this.zipcode = zipcode;
        this.priceDetail = priceDetail;
        this.transportsDetails = transportsDetails;
        this.contactUrl = contactUrl;
        this.coverUrl = coverUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventStartDate() {
        return eventStartDate;
    }

    public void setEventStartDate(String eventStartDate) {
        this.eventStartDate = eventStartDate;
    }

    public String getEventEndDate() {
        return eventEndDate;
    }

    public void setEventEndDate(String eventEndDate) {
        this.eventEndDate = eventEndDate;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(String priceDetail) {
        this.priceDetail = priceDetail;
    }

    public String getTransportsDetails() {
        return transportsDetails;
    }

    public void setTransportsDetails(String transportsDetails) {
        this.transportsDetails = transportsDetails;
    }

    public String getContactUrl() {
        return contactUrl;
    }

    public void setContactUrl(String contactUrl) {
        this.contactUrl = contactUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }
}
