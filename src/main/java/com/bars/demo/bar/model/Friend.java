package com.bars.demo.bar.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Friend_table")
public class Friend {

    @EmbeddedId
    private Key key = new Key();

    @ManyToOne
    @MapsId("userId")
    private User user;

    @ManyToOne
    @MapsId("friendId")
    private User friend;

    private boolean isActive = true;

    //Default constructor.
    public Friend(){}

    public Friend(User user,User friend){
        this.user = user;
        this.friend = friend;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getFriend() {
        return friend;
    }

    public void setFriend(User friend) {
        this.friend = friend;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Embeddable
    public static class Key implements Serializable {
        private Long userId;
        private Long friendId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getFriendId() {
            return friendId;
        }

        public void setFriendId(Long friendId) {
            this.friendId = friendId;
        }
    }
}
