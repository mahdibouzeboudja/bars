package com.bars.demo.bar.model;

import com.bars.demo.bar.Model;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="favorite_bar")
@Data
public class FavoriteBar extends Model {

    @NotNull
    public long eventId;
    @NotNull
    public long userId;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public long id;

    public FavoriteBar() {
    }

    public FavoriteBar(long eventId, long userId) {
        this.eventId = eventId;
        this.userId = userId;
    }

    public long getBar_id() {
        return eventId;
    }

    public void setBar_id(long eventId) {
        this.eventId = eventId;
    }

    public long getUser_id() {
        return userId;
    }

    public void setUser_id(int userId) {
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
