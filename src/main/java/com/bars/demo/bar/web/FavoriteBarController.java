package com.bars.demo.bar.web;

import com.bars.demo.bar.model.FavoriteBar;
import com.bars.demo.bar.repository.FavoriteBarRepository;
import com.bars.demo.bar.service.FavoriteBarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/favoriteBar")
public class FavoriteBarController {

    private FavoriteBarRepository favoriteBarRepository;
    private FavoriteBarService favoriteBarService;

    @Autowired
    public FavoriteBarController(FavoriteBarRepository favoriteBarRepository, FavoriteBarService favoriteBarService) {
        this.favoriteBarRepository = favoriteBarRepository;
        this.favoriteBarService = favoriteBarService;
    }

    @RequestMapping(method = GET, value = "/{id}")
    public ResponseEntity getById(@PathVariable long id)
    {
        FavoriteBar favoriteBar = this.favoriteBarRepository.getOne(id);
        if(favoriteBar != null){
            return ResponseEntity.ok(favoriteBar);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }

    @GetMapping
    public ResponseEntity getAll()
    {
        Iterable<FavoriteBar> favoriteBars = this.favoriteBarRepository.findAll();
        if (favoriteBars.iterator().hasNext()){
            return ResponseEntity.ok(favoriteBars);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }

    @RequestMapping(method = POST)
    public ResponseEntity createFavoriteBar(@RequestParam long eventId, @RequestParam long userId)
    {
        FavoriteBar favoriteBar = new FavoriteBar(eventId, userId);
        FavoriteBar favoriteBar1 = this.favoriteBarRepository.save(favoriteBar);
        if (favoriteBar1 != null){
            return ResponseEntity.ok(favoriteBar1);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }


    @RequestMapping(method = DELETE, value = "/{id}")
    public ResponseEntity deleteFavoriteBar(@RequestParam long id)
    {
        if(this.favoriteBarRepository.existsById(id)){
            this.favoriteBarRepository.deleteById(id);
            return ResponseEntity.ok(true);
        }
        else{
            return ResponseEntity.
                    status(HttpStatus.NOT_FOUND)
                    .build();
        }
    }
}
