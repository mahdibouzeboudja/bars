package com.bars.demo.bar.web;

import com.bars.demo.bar.model.Event;
import com.bars.demo.bar.service.EventService;
import com.bars.demo.bar.service.HTTPRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@SpringBootApplication
@RestController
@RequestMapping("/events")
public class EventsController {

    private EventService eventService;

    @Autowired
    public EventsController(EventService eventService){
        this.eventService = eventService;
    }

    @RequestMapping(method = GET, value = "/")
    public ResponseEntity getAll()
    {
        Iterable<Event> events = this.eventService.getAllEvents();

        if (events.iterator().hasNext()){
            return ResponseEntity.ok(events);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }

    @RequestMapping(method = GET, value ="/geolocation")
    public ResponseEntity getByGeolocation(@RequestParam double latitude, @RequestParam Double longitude, @RequestParam int distance)
    {
        Iterable<Event> events = this.eventService.getEventsByGeolocation(latitude, longitude, distance);

        if (events.iterator().hasNext()){
            return ResponseEntity.ok(events);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }

    @RequestMapping(method = GET, value ="/geolocation/{id}")
    public ResponseEntity getByGeolocation(@PathVariable int id)
    {
        Event event = this.eventService.getEventsById(id);

        if (event != null){
            return ResponseEntity.ok(event);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }
}
