package com.bars.demo.bar.web;

import com.bars.demo.bar.model.Friend;
import com.bars.demo.bar.model.User;
import com.bars.demo.bar.repository.FriendRepository;
import com.bars.demo.bar.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@RestController
public class FriendController {

    private UserRepository userRepository;
    private FriendRepository friendRepository;

    @Autowired
    public FriendController(UserRepository userRepository, FriendRepository friendRepository) {
        this.userRepository = userRepository;
        this.friendRepository = friendRepository;
    }

    @PatchMapping("/friends/{receiver_id}")
    ResponseEntity addFreind(@RequestBody Map<String, Long> requester_id, @PathVariable long receiver_id) {
        User requester = this.userRepository.findById(requester_id.get("user_requester")).get();
        User receiver = userRepository.findById(receiver_id).get();
        receiver.addFriend(new Friend(receiver, requester));
        userRepository.save(receiver);
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body("Relationship created");
    }

    @GetMapping("/friends/{id}")
    ResponseEntity getFriends(@PathVariable long id){
        User user = this.userRepository.findById(id).isPresent() ? userRepository.findById(id).get(): null;
        Set<Friend> friends = friendRepository.findAllByUser(user);
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(friends);
    }
}

