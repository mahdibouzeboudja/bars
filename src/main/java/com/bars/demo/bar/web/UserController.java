package com.bars.demo.bar.web;

import com.bars.demo.bar.dto.UserDTO;
import com.bars.demo.bar.model.User;
import com.bars.demo.bar.repository.UserRepository;
import com.bars.demo.bar.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserRepository userRepository;

    private UserService userService;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserController(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UserService userService){
        this.userRepository =userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userService = userService;
    }

    @GetMapping
    ResponseEntity getAll(){
        Iterable<User> users = this.userRepository.findAll();
        if (users.iterator().hasNext()){
            return ResponseEntity.ok(users);
        }
        return ResponseEntity.
                status(HttpStatus.NO_CONTENT)
                .build();
    }

    @GetMapping("/search/{username}")
    ResponseEntity getUserByUsername(@PathVariable String username){
        User user = this.userRepository.findByUsername(username);
        if (user != null){
            return ResponseEntity.ok(user);
        }
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body("User with pseudo " + username + " not found");
    }

    @GetMapping("/{id}")
    ResponseEntity getUserByID(@PathVariable long id){
        Optional<User> user = this.userRepository.findById(id);
        if (user.isPresent()){
            return ResponseEntity.ok(user.get());
        }
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body("User with id " + id + " not found");
    }

    @PostMapping("/signup")
    public boolean signUp(@RequestBody User user) {
        if(!this.userRepository.existsByUsername(user.getUsername())) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteUser(@PathVariable Long id){
        Optional<User> user = this.userRepository.findById(id);
        if (user.isPresent()) {
            this.userRepository.delete(user.get());
            return ResponseEntity
                    .status(HttpStatus.ACCEPTED)
                    .body("User successfully deleted");
        }
        return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body("User not deleted");
    }

    @PostMapping("/login")
    ResponseEntity login(@RequestBody UserDTO user){
        String username = user.getUsername();
        String password = user.getPassword();
        try {
            String jwttoken = this.userService.signIn(username,password);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header("Authorization", "Bearer " + jwttoken)
                    .build();
        }catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(e.getMessage());
        }
    }
}

