package com.bars.demo.bar.web;

import com.bars.demo.bar.model.Mark;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@Controller
@SpringBootApplication
@RestController
@RequestMapping("/mark/{markId}")
@ResponseBody
public class MarkController {

    Mark mark = new Mark();

    @RequestMapping(method = GET)
    public Mark getMarkById(@PathVariable final Integer markId){
        mark.setId(markId);
        return mark;
    }


}
