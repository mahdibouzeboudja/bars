package com.bars.demo.bar.web;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@Controller
@SpringBootApplication
@RestController
@RequestMapping("/healthcheck")
@ResponseBody
public class HealthCheck {

    @RequestMapping(method = GET)
    public String returnMsg(){
        return "health";
    }
    public ResponseEntity sendViaResponseEntity() {
        return new ResponseEntity(HttpStatus.OK);
    }


}
