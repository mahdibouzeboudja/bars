package com.bars.demo.bar.repository;

import com.bars.demo.bar.model.FavoriteBar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FavoriteBarRepository extends JpaRepository<FavoriteBar, Long> {
}
