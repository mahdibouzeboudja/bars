package com.bars.demo.bar.repository;

import com.bars.demo.bar.model.Mark;
import com.bars.demo.bar.model.User;

import java.util.List;

public interface MarkRepository {

    Mark getMarkById(Integer MarkId);

    void addId(Mark mark);

    List<Mark> getAllMark();

}
